Name:		perl-Net-SSLeay
Version:	1.94
Release:	1
Summary:	Perl module for using OpenSSL
License:	Artistic 2.0
URL:		https://metacpan.org/release/Net-SSLeay
Source0:        https://cpan.metacpan.org/authors/id/C/CH/CHRISN/Net-SSLeay-%{version}.tar.gz

# Build requires
BuildRequires:	openssl-devel perl-devel perl-generators gcc bc openssl zlib-devel
# Test Suite requires
BuildRequires:	 perl-Test-NoWarnings perl-Test-Pod
# Runtime requires
Requires:       perl perl-MIME-Base64

%description
This Package provides perl modules(Net::SSLeay and Net::SSLeay::Handle) for using OpenSSL.

%package_help

%prep
%autosetup -n Net-SSLeay-%{version} -p1

rm -f examples/cb-testi.pl examples/server_key.pem

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc Changes Credits README CONTRIBUTING.md
%{perl_vendorarch}/auto/Net/*
%{perl_vendorarch}/Net/SSLeay*

%files help
%{_mandir}/man3/Net::SSLeay*
%doc QuickRef examples/

%changelog
* Wed Jan 31 2024 woody2918 <wudi1@uniontech.com> - 1.94-1
- update version to 1.94
- supports all stable releases of OpenSSL 3.1 and 3.2, and LibreSSL 3.5 - 3.8.
- Enhancements to the API.

* Mon Feb 6 2023 wangyuhang <wangyuhang27@huawei.com> - 1.92-2
- add buildrequire: zlib-devel

* Wed Oct 26 2022 wangyuhang <wangyuhang27@openeuler.org> - 1.92-1
- update version to 1.92

* Tue Jun 28 2022 shixuantong <shixuantong@h-partners.com> - 1.90-2
- fix Failed test CIPHER_get_version

* Mon Dec 20 2021 shangyibin <shangyibin1@openeuler.org> - 1.90-1
- DESC:update version to 1.90

* Tue Jun 29 2021 wuchaochao <wuchaochao4@huawei.com> - 1.88-6
- add buildrequers : bc, gcc, openssl

* Mon Feb 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.88-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete pem file

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.88-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Mon Oct 28 2019 shenyangyang <shenyangyang4@huawei.com> - 1.88-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires of perl-Test-Warn and perl-Test-Exception

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.88-2
- Adjust requires

* Sat Aug 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.88-1
- Package init
